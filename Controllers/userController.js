const mongoose = require("mongoose");

const User = require("../Models/userSchema");
const Course = require("../Models/coursesSchema")

const bcrypt = require("bcrypt");
 
const auth =require("../auth.js")

// Controllers

// This controller will create or register a user on our database

module.exports.userRegistration = (request, response)=>{
    const input = request.body;
    User.findOne({email: input.email})
    .then(result =>{
        if(result !== null){
            return response.send("The email is already taken")
        }else{
            let newUser = new User({
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                password: bcrypt.hashSync(input.password, 10),
                mobileNumber: input.mobileNumber
            })
            //save to database
            newUser.save()
            .then(save =>{
                return response.send("You are now registered to our website")

            }).catch(error =>{
                return response.send(error)
            })
        }
    }).catch(error=>{
        return response.send(error)
    })
}


module.exports.userAuthentication = (request, response)=>{

    let input = request.body;

    //possible scenarios in logging in
    //1. email is not yet registered
    //2. email is registered but the password is wrong

    User.findOne({email: input.email})
    .then(result =>{
        if(result === null){
            return response.send("Email is not yet registered! Register first before logging in!")
        }else{
            const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
        if(isPasswordCorrect){
            return response.send({auth: auth.createAccessToken(result)})

        }else{
            return response.send("Password is incorrect!")
        }
       
        }


    }).catch(error =>{
        return response.send(error)
    })
}

module.exports.getProfile = (request, response)=>{

    // let input = request.body;
    const userData =auth.decode(request.headers.authorization)

    console.log(userData);
    User.findById(userData._id)
    .then(result =>{
        
        result.password="**********"
        
        return response.send(result) 
    })
    .catch(error =>{
        return response.send(error)
    })

}

// Controller for user enrollment
//1. we can get the id of the user by decoding the jwt

//2. we can get the courseId by using the request params

module.exports.enrollCourse = async (request, response) =>{
    //first we have to get the user ID and the course id

    const userData = auth.decode(request.headers.authorization);
    // get the courseId by targetting the params in the url
    const courseId = request.params.courseId;
    
    let idFind =  await Course.findById(courseId)
    console.log(idFind)

     if(idFind === null || userData.isAdmin == true ){
            return response.send("You dont have access to this route!")
        }else{
    
                    let isUserUpdated = await User.findById(userData._id)
                    .then(result =>{
                        if(result === null){
                            return false
                        }else{
                        result.enrollments.push({courseId: courseId})
                        return result.save()
                        .then(save => true)
                        .catch(error => false)
                        }
                    })
    
    
    
                        let isCourseUpdated =  await Course.findById(courseId)
                        .then(result =>{
                            if(result === null){
                                return false
                            }else{
                                
                                result.enrolles.push({userId:userData._id})
                                
                            return result.save()
                                .then(save => true) 
                                .catch(error => false)
                            }
    
                        })
    
                        console.log(isCourseUpdated);
                        console.log(isUserUpdated);
                        
                        if(isCourseUpdated && isUserUpdated){
                            return response.send("The course is now enrolled!")
                        }else{
                            return response.send("There was an error during the enrollment. Please try again")
                        }
    
                    }
                 
                    
 }
        
    
    













