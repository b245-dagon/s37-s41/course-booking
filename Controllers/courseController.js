const Course =require("../Models/coursesSchema.js");

const admin = require("../Models/userSchema");
const auth =require("../auth.js");
const { response } = require("express");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/
module.exports.addCourse = (request, response)=>{
    const adminData =auth.decode(request.headers.authorization)
    let input= request.body
    console.log(adminData)
        if(adminData.isAdmin == true){
            let newCourse = new Course({
                name: input.name,
                description: input.description,
                price: input.price
                })
            
        return newCourse.save()
        // couse succesfully created
        .then(result =>{
            response.send("Successfully added");
        }).catch(error=>{
            console.log(error);
            response.send(false)
        })
            
        }else{ 
            return response.send("You are not admin")
    
        }
    }

    //saves the created object to our database   

module.exports.allCourses = (request, response) =>{
    const userData = auth.decode(request.headers.authorization)
    if (!userData.isAdmin){
        return response.send("You dont have access to this route!")

    }else{
        Course.find({}).then(result =>{
            response.send(result)
        }).catch(error => response.send(error))
    }
}

module.exports.allActiveCourses = (request, response) =>{
    Course.find({isActive:true})
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

module.exports.allInactiveCourses = (request, response)=>{
    const userData = auth.decode(request.headers.authorization)
    console.log(userData)
    
    if(!userData.isAdmin){
        return response.send("You dont have access to this route!")
    }
    else{
    Course.find({isActive: false})
    .then(result => response.send(result))
    .catch(error => response.send(error))
    }
}

module.exports.courseDetails = (request,response)=>{
    const courseId = request.params.courseId;

    Course.findById(courseId)
    .then(result => response.send(result))
    .catch(error => response.send(error))

}

module.exports.updateCourse = (request, response) =>{
    const userData = auth.decode(request.headers.authorization)

    const CourseId = request.params.courseId;

    const input = request.body

    if(!userData.isAdmin){
        return response.send("You dont have access to this route!")
    }else{
     Course.findOne({_id:CourseId})
     .then(result =>{
        if(result === null){
            return response.send("Course Id is invalid try again")
        }else{
            let updatedCourse = {
                name: input.name,
                description: input.description,
                price: input.price
               }
               
                Course.findByIdAndUpdate(CourseId,
                    updatedCourse, {new:true})
                    .then(result => {
                        console.log(result)
                        return response.send(result)})
                    .catch(error => response.send(error))
        }
     }).catch(error =>{
        return response.send(error)
     })
    }

}
module.exports.courseArchive = (request,response) => {
    const userData = auth.decode(request.headers.authorization);


    const CourseId = request.params.courseId;

    const input = request.body
    if(!userData.isAdmin){
        return response.send("You dont have access to this route!")
    }else{
     Course.findOne({_id:CourseId})
     .then(result =>{
        if(result === null){
            return response.send("Course Id is invalid try again")
        }else{
            let archiveCourse = {
                isActive : input.isActive
               }
               
                Course.findByIdAndUpdate(CourseId,
                    archiveCourse, {new:true})
                    .then(result => {
                        console.log(result)
                        return response.send(result)})
                    .catch(error => response.send(error))
        }
     }).catch(error =>{
        return response.send(error)
     })
    }



}