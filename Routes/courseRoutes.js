const express = require("express");
const router = express.Router();

const courseController = require('../Controllers/courseController');
const auth = require("../auth.js")


//Route for creating course

router.post("/",auth.verify, courseController.addCourse);



router.get("/all",auth.verify, courseController.allCourses);

router.get("/allActive",courseController.allActiveCourses);

router.get("/allInactive",auth.verify,courseController.allInactiveCourses);

//routes for retrieving details

router.get("/:courseId",courseController.courseDetails)

router.put("/update/:courseId",auth.verify,courseController.updateCourse)

router.put("/archive/:courseId",auth.verify,courseController.courseArchive)

module.exports = router;

