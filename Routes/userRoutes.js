const express = require("express")

const router = express.Router();
const userController = require("../Controllers/userController");

const auth = require("../auth.js")

//[Routes]

router.post("/register",userController.userRegistration);


//this route is for the user authentication

router.post("/login", userController.userAuthentication);


router.get("/details",auth.verify,userController.getProfile);


router.post("/enroll/:courseId" , auth.verify,userController.enrollCourse)




module.exports = router;