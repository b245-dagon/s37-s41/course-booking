const express = require("express");


const mongoose = require ("mongoose");
//by default our backend's CORS setting will prevent any application outside Express JS appto precess the request.
// Using the cores package, it will allow us to manipulate this and control wht application may use our app.
// allows our backend application to be available to our frontend application
// Allows us to control ehe apps Cross Origin 
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js")
const courseRoutes = require("./Routes/courseRoutes.js")
const port = 3001;

const app = express();

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

//routing

app.use("/user",userRoutes);
app.use("/course",courseRoutes);

mongoose.set("strictQuery", true);

mongoose.connect("mongodb+srv://admin:admin@batch245-dagon.wirp2aj.mongodb.net/batch245_Course_API_Dagon?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console,"Connection Error!"));
db.once("open", ()=>{
    console.log("We are Connected to the cloud")
})



app.listen(port,()=> console.log(`Server is running at Port ${port}`));