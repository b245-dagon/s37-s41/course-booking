const jwt = require ("jsonwebtoken")

//user defiend string data that will be used to create JSON web tokens
//used in the algo for encrypting our data which makes it difficult to decode the information without defined secret keyword


const secret = "CourseBookingAPI";
//[Section] JSON web token
    //JSON web token or jwt is a way of securely passing the server to the front end or the other parts of the server.
    
    //information is kept secure through the use of the secret code
    //only the system will know the secret code that can decode the encrypted information
    
//Token creation
// analogy: 



module.exports.createAccessToken = (user) =>{
    const data = {
        //payload
        _id: user.id,
        email: user.email,
        isAdmin: user.isAdmin

    }
    //.sign ()from jwt package will generate a JSON web token.
    //Syntax:
        //jwt.sign(payload,secretcode,options)
    return jwt.sign(data, secret,{});

}

//token verifivation
// analogy
    //-Received the gift and open the lock to verify if the sender is legitimate and the gift was not tampered.
//middleware  function have access with request object and response object
// "next" function indicates that we may proceed with the next step
module.exports.verify = (request, response, next)=>{
    // Token is retrieved from the request headers
    // this can be provided in postman under
        //authorization  > Bearer Token

        let token = request.headers.authorization;

       

        // Token recieved and is not undefined

        if(typeof token !== "undefined"){   
            //retrieves only token and removes the"Beater" prefix
            token=token.slice(7, token.length);
            console.log(token);

            // validate the token using the "verify" method decrypting the token using the secret code.
            //jwt.verify(token , secretOrPrivateKay,[options/callbackFunction])
            return jwt.verify(token,secret,(err,data)=>{
                if(err){
                    return response.send({auth:"Failed."})
                }else{
                    next();
                }
            })
        }else{
            return response.send({auth: "Failed"})
        }

}
//Token decryption
    //Analogy
        //- Open the gift and get the content.

module.exports.decode = (token) => {
    //token receive is not undefined 
    if(typeof token !== "undefined"){
        token = token.slice(7,token.length);

        return jwt.verify(token, secret, (err, data)=>{
            if(err){
                return null;
            }else{
                // The "decode" method is used to obtain information from the JWT
                // jwt.decode(token,[options])
                //
                return jwt.decode(token,{complete:true}).payload;
            }
        })

    }else{
        return null;
    }
}